#Errik's Road by Marble Groove

import random
import time

def start():
    print("")
    print("Welcome to Errik's Road: the tale of a good-hearted Lumberjack")
    print("")
    print("Pay very close attention to the situations in which you find yourself.\nFollow the CHERRY PIES, and don't get yourself killed!")
    game_start()


def game_start(): # intro narration
    print("")
    answer_bed = input("Our story begins with you, Errik, asleep in your cabin's bed.\nYou feel the warmth of the morning sun pouring in through the window.\nDo you awaken and stand, or do you make an attempt to return to sleep?\n\n(stand/sleep) ")
    print("")
    if (answer_bed == "sleep") :
        sleep()
    if answer_bed == "stand":
        stand()
    else:
        print("I'm sorry, what?")

def sleep(): # decide to go to sleep
    print("")
    answer_dream = input("You decide to close your eyes and you return to sleep. While you're resting, you begin to DREAM.\nYou're now aimlessly wandering through a dense forest. There is no path. With each step you take, your HUNGER grows.\nEventually, you stumble upon a clearing, and you see a familiar looking CABIN in the clearing.\nDo you approach the CABIN?\n\n(approach/do not) ")
    print("")
    if answer_dream == "approach":
        approach()
    if answer_dream == "do not":
        do_not()

def approach(): # approach dream cabin
    print("")
    answer_approach = input("You approach the CABIN in the clearing. You can see a brick CHIMNEY adorning the roof with smoke billowing out of the top. There is nobody around.\nThere is a locked DOOR and a single pane WINDOW. Looking through the window, you see a freshly baked CHERRY PIE on the floor of the cabin.\nWhat do you do?\n\n(door/window/explore) ")
    print("")
    if answer_approach == "door":
        door_locked()
    if answer_approach == "window":
        window_strong()
    if answer_approach == "explore":
        explore()

def door_locked(): # door is locked
    print("")
    answer_door_locked = input("You try to open the DOOR, but it is locked. You see no possible way of getting the DOOR open.\n\n(window/explore) ")
    print("")
    if answer_door_locked == "window":
        window_strong()
    if answer_door_locked == "explore":
        explore()

def window_strong(): # you're better than that
    print("")
    answer_window = input("You turn your attention towards the large, single pane WINDOW. Your HUNGER tries to get you to break open the window, but you're better than that.\n\n(door/explore) ")
    print("")
    if answer_window == "door":
        door_locked()
    if answer_window == "explore":
        explore()

def explore(): # look around the cabin
    print("")
    answer_explore = input("Whether it be your sense of curiosity or hunger, something tells you to explore behind the CABIN.\nBehind the cabin, you see a large stack of chopped firewood, a barrel, and a chest full of tools.\nOn top of the barrel is a small, perfectly-wrapped GIFT BOX with your name on it.\nYou pick up the box and open it. Inside is a POTION and a COOKIE.\n\n(potion/cookie/no thanks) ")
    print("")
    if answer_explore == "no thanks":
        do_not()
    if answer_explore == "potion":
        drink_shrink()
    if answer_explore == "cookie":
        big_cookie()

def drink_shrink(): # drink potion, is small
    print("")
    print("The potion vial contains a pale blue, swirling liquid. It is capped off by a cork and is adorned by a note which reads <-DRINK ME->.\nBeing the curious epicurean that you are, you pop off the cork and heartily quaff the potion.")
    print(boom_symbol)
    answer_drink = input("In a flash of silver smoke and a crash of lightning, your body is\nshrunk down to a very small size - you're not much bigger than a mouse!\nYou scale the stack of chopped firewood to the top of the cabin's roof.\nThe scent of CHERRY PIE is getting stronger!\nWill you inspect the chimney or look elsewhere?\n\n(chimney/elsewhere) " )
    if answer_drink == "chimney":
        chimney()
    if answer_drink == "elsewhere":
        wall_hole()

def chimney(): # chimney death
    print("")
    print("You walk over to the mouth of the chimney and for some reason, decide to jump down.\nIt is a quick and smoky fall to a roaring fire down below. You frantically try to escape from the blazing firepit, but you don't.")
    print("")
    scary_wakeup()

def wall_hole(): # hole in wall leading to cherry pie
    print("")
    print("You decide to explore the roof of the log cabin. You notice a hole in the roof and decide to peer into it.\nThankfully, nothing bad happens!\n\nLooking around inside the cabin, you see SHELVES directly under you, and a curiously well-stocked kitchen for this type of dwelling.")
    answer_wall_hole = input("You see the CHERRY PIE on the floor of the cabin.\nWhy is it on the floor? Don't ask silly questions!\nYour hunger drives you to leap into the unknown situation below, as you jump from shelf to shelf onto the counter.\nWith each shelf, you hear a SQUEAKING sound, danger could be present.\nDo you arm yourself?\n\n(weapon/proceed) ")
    if answer_wall_hole ==  "weapon":
        cheese_knife()
    if answer_wall_hole == "proceed":
        floor_unarmed()

def cheese_knife():
    print("")
    print("A mysterious cabin in the woods with sneaky squeakies in the walls? Sounds like it could get hairy.\nYou see a CHEESE KNIFE stuck in a nearby charcuterie board.\nYou manage to pull it out, and you now have a weapon! ")
    print("")
    answer_cheese_knife_name = input("What would you like to name your weapon? ")
    print("You leap off the counter onto the dining table, and make your way onto the floor. The CHERRY PIE is SO CLOSE!\nYou begin to approach the pie when you hear a stampede of paws and squeaks!\nYou have trespassed in the domain of PUMPERNICKEL, THE RAT KING!")
    print("")
    answer_cheese_knife = input("")
    if answer_cheese_knife == "attack":
        attack_rat()
    if answer_cheese_knife == "diplomacy":
        diplomacy()
    if answer_cheese_knife == "trade":
        trade()

def attack_rat():
    print("")
    print("Without hesitation, you lunge at PUMPERNICKEL, THE RAT KING with your cheese knife")
    print("")

def diplomacy():
    pass

def trade():
    pass

def floor_unarmed():
    pass

def big_cookie(): # eat cookie, is big
    print("")
    print("")
    print("")

def stand(): # stand up, awake scenario A
    pass

def do_not(): # return to forest and die
    print("")
    print("You decide your time is best spent elsewhere. You return to wander the FOREST.\nIt is getting very dark very quickly. You hear lots of giggling, and the smell of rotting vegetation fills the air.\nThis can only mean that PUMPKIN GOBLINS are nearby. Thank goodness this is just a dream.\nSuddenly, you begin freefalling into the darkness feet first.\nYou have been consumed by THE VOID. ")
    scary_wakeup()

def scary_wakeup(): # scary wakeup, begin scenario 2
    print("")
    answer_scary_wakeup = input("The sensation of suddenly falling to your certain death jolts you awake.\nYou sit up in your bed, breathing heavily and drenched in sweat. What a crazy dream.\nDo you want MORE sleep, or is it time to wake up?\n\n(sleep again/stand) ")
    print("")
    if answer_scary_wakeup == "sleep again":
        sleep_again()
    if answer_scary_wakeup == "stand":
        stand()

def happy_wakeup(): # happy wakeup, begin scenario 2
    pass

def sleep_again(): # sleep #2 leading to bad ending
    pass

boom_symbol = "\n  ╱﹌﹌﹌﹌﹌﹌﹌﹌﹌﹌﹌╲\n  ╲  ╔══╗╔═══╦═══╦═╗╔═╗  ╱\n  ╱  ║╔╗║║╔═╗║╔═╗║║╚╝║║  ╲\n  ╲  ║╚╝╚╣║─║║║─║║╔╗╔╗║  ╱\n  ╱  ║╔═╗║║─║║║─║║║║║║║  ╲\n  ╲  ║╚═╝║╚═╝║╚═╝║║║║║║  ╱\n  ╱  ╚═══╩═══╩═══╩╝╚╝╚╝  ╲\n  ╲﹏﹏﹏﹏﹏﹏﹏﹏﹏﹏﹏╱ \n"

start()